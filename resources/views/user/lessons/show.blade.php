@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">{{ __('entities.lesson') }} {{ $lesson->season->title }} </div>
                    <div class="card-body">

                        <a href="{{ url('/user/lessons') }}" title="Back">
                            <button class="btn btn-outline-primary btn-sm"><i class="fa fa-arrow-left"
                                                                              aria-hidden="true"></i>
                                {{ __('entities.back') }}
                            </button>
                        </a>
                        <a href="{{ url('/user/lessons/' . $lesson->id . '/edit') }}" title="Edit Lesson">
                            <button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"
                                                                      aria-hidden="true"></i> {{ __('entities.edit') }}
                            </button>
                        </a>

                        <form method="POST" action="{{ url('user/lessons' . '/' . $lesson->id) }}"
                              accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-outline-primary btn-sm" title="Delete Lesson"
                                    onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o"
                                                                                             aria-hidden="true"></i>
                                {{ __('entities.delete') }}
                            </button>
                        </form>
                        <br/>
                        <br/>
                        <h3 class="mt-4">{{ __('content.group_lessons') }}</h3>
                        <div class="row">
                            <div class="col">
                                <div class="table-responsive">
                                    <table class="table table-sm">
                                        <tbody>
                                        <tr>
                                            <th> {{ __('entities.ski_kids') }}</th>
                                            <td> {{ $lesson->ski_kids }} </td>
                                        </tr>
                                        <tr>
                                            <th> {{ __('entities.snowboard_kids') }}</th>
                                            <td> {{ $lesson->snowboard_kids }} </td>
                                        </tr>
                                        <tr>
                                            <th> {{ __('entities.other_kids') }}</th>
                                            <td> {{ $lesson->other_kids }} </td>
                                        </tr>
                                        <tr>
                                            <th> {{ __('entities.total_lessons_kids') }}</th>
                                            <td> {{ $lesson->total_kids_lessons }} </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col">
                                <div class="table-responsive">
                                    <table class="table table-sm">
                                        <tbody>
                                        <tr>
                                            <th> {{ __('entities.ski_adults') }}</th>
                                            <td> {{ $lesson->ski_adults }} </td>
                                        </tr>
                                        <tr>
                                            <th> {{ __('entities.snowboard_adults') }}</th>
                                            <td> {{ $lesson->snowboard_adults }} </td>
                                        </tr>
                                        <tr>
                                            <th> {{ __('entities.other_adults') }}</th>
                                            <td> {{ $lesson->other_adults }} </td>
                                        </tr>
                                        <tr>
                                            <th> {{ __('entities.total_lessons_adults') }}</th>
                                            <td> {{ $lesson->total_adults_lessons }} </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <h3 class="mt-4">{{ __('content.private_lessons') }}</h3>
                        <div class="row">
                            <div class="col-6">
                                <div class="table-responsive">
                                    <table class="table table-sm">
                                        <tbody>
                                        <tr>
                                            <th> {{ __('entities.other_private') }}</th>
                                            <td> {{ $lesson->other_private }} </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <h3 class="mt-4">{{ __('content.turnover') }}</h3>
                        <div class="row">
                            <div class="col">
                                <div class="table-responsive">
                                    <table class="table table-sm">
                                        <tbody>
                                        <tr>
                                            <th> {{ __('entities.turnover_private') }}</th>
                                            <td> {{ $lesson->turnover_private }} </td>
                                        </tr>
                                        <tr>
                                            <th> {{ __('entities.turnover_group') }}</th>
                                            <td> {{ $lesson->turnover_group }} </td>
                                        </tr>
                                        <tr>
                                            <th> {{ __('entities.turnover_other') }}</th>
                                            <td> {{ $lesson->turnover_other }} </td>
                                        </tr>
                                        <tr>
                                            <th> {{ __('content.total') }}</th>
                                            <td> {{ $lesson->total_turnover }} </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <h3 class="mt-4">{{ __('content.salary_sum') }}</h3>
                        <div class="row">
                            <div class="col-6">
                                <div class="table-responsive">
                                    <table class="table table-sm">
                                        <tbody>
                                        <tr>
                                            <th> {{ __('entities.total_salary') }}</th>
                                            <td> {{ $lesson->total_salary }} </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
