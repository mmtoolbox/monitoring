@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('includes.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">{{ __('entities.lesson') }}</div>
                    <div class="card-body">
                        <a href="{{ url('/user/lessons/create') }}" class="btn btn-primary btn-sm" title="Add New Lesson">
                            <i class="fa fa-plus" aria-hidden="true"></i> {{ __('entities.add_new') }}
                        </a>

                        <form method="GET" action="{{ url('/user/lessons') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="{{ __('entities.search') }}..." value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>


                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th><th>{{ __('entities.season') }}</th><th class="text-right">{{ __('entities.actions') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($lessons as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->season->title }}</td>
                                        <td class="text-right">
                                            <a href="{{ url('/user/lessons/' . $item->id) }}" title="View Lesson"><button class="btn btn-outline-primary btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> {{ __('entities.view') }}</button></a>
                                            <a href="{{ url('/user/lessons/' . $item->id . '/edit') }}" title="Edit Lesson"><button class="btn btn-outline-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ __('entities.edit') }}</button></a>

                                            <form method="POST" action="{{ url('/user/lessons' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-outline-primary btn-sm" title="Delete Lesson" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> {{ __('entities.delete') }}</button>
                                            </form>


                                            <a href="{{ url('/user/lessons/' . $item->id . '/export') }}"
                                               title="{{__('entities.export') }}">
                                                <button class="btn btn-outline-primary btn-sm"><i
                                                        class="fa fa-file-excel-o"
                                                        aria-hidden="true"></i> {{__('entities.export') }}</button>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $lessons->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
