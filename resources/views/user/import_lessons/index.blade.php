@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('includes.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">{{ __('import.import') }}</div>
                    <div class="card-body">
                        <form class="form-horizontal" method="POST" action="{{ route('import_upload') }}"
                              enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('csv_file') ? ' has-error' : '' }}">
                                <label for="csv_file" class="">{{ __('import.file_to_import') }}</label>

                                <input id="csv_file" type="file" class="form-control-file" name="csv_file" required>

                                @if ($errors->has('csv_file'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('csv_file') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <div class="">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('import.import_csv') }}
                                    </button>
                                </div>
                            </div>

                    </div>

                    </form>


                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
