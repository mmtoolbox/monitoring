@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('includes.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">{{ __('import.import') }}</div>
                    <div class="card-body">
                        <h2>{{ __('import.file_imported') }}</h2>

                        @foreach($successful as $success)
                            <div class="alert alert-success" role="alert">
                                {!! $success !!}
                            </div>
                        @endforeach

                        @foreach($errors as $error)
                            <div class="alert alert-danger" role="alert">
                                {!! $error !!}
                            </div>
                        @endforeach
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
