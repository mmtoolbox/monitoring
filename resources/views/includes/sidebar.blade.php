<div class="col-md-3 mb-4">
    <div class="card">
        <div class="card-header">
            Navigation
        </div>

        <div class="card-body">
            <ul class="nav nav-pills flex-column" role="tablist">
                @can('isAdmin')
                    <li role="presentation" class="nav-item border-bottom">
                        <a href="{{ url('/home') }}" class="nav-link">
                            {{ __('content.home') }}
                        </a>
                    </li>
                    <li role="presentation" class="nav-item border-bottom">
                        <a href="{{ url('/admin/accident') }}" class="nav-link">
                            {{ __('entities.accidents_overview') }}
                        </a>
                    </li>
                    <li role="presentation" class="nav-item border-bottom">
                        <a href="{{ url('/admin/lessons') }}" class="nav-link">
                            {{ __('entities.lesson') }}
                        </a>
                    </li>
                    <li role="presentation" class="nav-item border-bottom">
                        <a href="{{ url('/admin/user') }}" class="nav-link">
                            {{ __('entities.schools') }}
                        </a>
                    </li>
                    <li role="presentation" class="nav-item border-bottom">
                        <a href="{{ url('/admin/seasons') }}" class="nav-link">
                            {{ __('entities.season') }}
                        </a>
                    </li>
                    <li role="presentation" class="nav-item border-bottom">
                        <a href="{{ url('/admin/weather') }}" class="nav-link">
                            {{ __('entities.weather') }}
                        </a>
                    </li>
                    <li role="presentation" class="nav-item border-bottom">
                        <a href="{{ url('/admin/niveau') }}" class="nav-link">
                            {{ __('entities.niveau') }}
                        </a>
                    </li>
                    <li role="presentation" class="nav-item border-bottom">
                        <a href="{{ url('/admin/accident-place') }}" class="nav-link">
                            {{ __('entities.accident_place') }}
                        </a>
                    </li>
                    <li role="presentation" class="nav-item border-bottom">
                        <a href="{{ url('/admin/weather') }}" class="nav-link">
                            {{ __('entities.weather') }}
                        </a>
                    </li>
                    <li role="presentation" class="nav-item border-bottom">
                        <a href="{{ url('/admin/snow-condition') }}" class="nav-link">
                            {{ __('entities.snow-condition') }}
                        </a>
                    </li>
                    <li role="presentation" class="nav-item border-bottom">
                        <a href="{{ url('/admin/accident-type') }}" class="nav-link">
                            {{ __('entities.accident-type') }}
                        </a>
                    </li>
                    <li role="presentation" class="nav-item border-bottom">
                        <a href="{{ url('/admin/piste-rescue') }}" class="nav-link">
                            {{ __('entities.piste-rescue') }}
                        </a>
                    </li>
                    <li role="presentation" class="nav-item border-bottom">
                        <a href="{{ url('/admin/accident-gravity') }}" class="nav-link">
                            {{ __('entities.accident-gravity') }}
                        </a>
                    </li>
                    <li role="presentation" class="nav-item border-bottom">
                        <a href="{{ url('/admin/protection') }}" class="nav-link">
                            {{ __('entities.protection') }}
                        </a>
                    </li>
                    <li role="presentation" class="nav-item border-bottom">
                        <a href="{{ url('/admin/injury-type') }}" class="nav-link">
                            {{ __('entities.injury-type') }}
                        </a>
                    </li>
                    <li role="presentation" class="nav-item border-bottom">
                        <a href="{{ url('/admin/body-part') }}" class="nav-link">
                            {{ __('entities.body-part') }}
                        </a>
                    </li>
                    <li role="presentation" class="nav-item border-bottom">
                        <a href="{{ url('/admin/discipline') }}" class="nav-link">
                            {{ __('entities.discipline') }}
                        </a>
                    </li>
                    <li role="presentation" class="nav-item border-bottom">
                        <a href="{{ url('/admin/age-category') }}" class="nav-link">
                            {{ __('entities.age-category') }}
                        </a>
                    </li>
                    <li role="presentation" class="nav-item">
                        <a href="{{ url('/admin/accident-activity') }}" class="nav-link">
                            {{ __('entities.accident-activity') }}
                        </a>
                    </li>
                @elsecan('isSchool')
                    <li role="presentation" class="nav-item border-bottom">
                        <a href="{{ url('/home') }}" class="nav-link">
                            {{ __('content.home') }}
                        </a>
                    </li>
                    <li role="presentation" class="nav-item border-bottom">
                        <a href="{{ url('/user/lessons') }}" class="nav-link">
                            {{ __('entities.lesson') }}
                        </a>
                    </li>
                    <li role="presentation" class="nav-item border-bottom">
                        <a href="{{ url('/user/accident') }}" class="nav-link">
                                {{ __('entities.accidents') }}
                        </a>
                    </li>
                    <li role="presentation" class="nav-item">
                        <a href="{{ url('/user/import') }}" class="nav-link">
                                {{ __('import.import') }}
                        </a>
                    </li>
                @else
                    <li role="presentation" class="nav-item">
                        <a href="{{ url('/login') }}" class="nav-link">
                            Login
                        </a>
                    </li>
                @endcan
            </ul>
        </div>
    </div>
</div>
