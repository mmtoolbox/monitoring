{!! __('entities.lessons_example_text')   !!}
<div class="form-group {{ $errors->has('season_id') ? 'has-error' : ''}}">
    <label for="season_id" class="control-label">{{ __('entities.season') }}</label>
    @if ($formMode === 'edit')
        <div>{{ $lesson->season->title }}</div>
    @else
        <select class="form-control" name="season_id">
            @foreach($seasons as $id => $title)
                <option value="{{$id}}" {{ isset($lesson) && $lesson->season_id == $id ? 'selected' : '' }}>{{$title}}
                </option>
            @endforeach
        </select>
        {!! $errors->first('season_id', '<p class="help-block">:message</p>') !!}
    @endif
</div>
<h3 class="mt-4">{{ __('content.group_lessons') }}</h3>
<div class="row">
    <div class="col">
        <div class="form-group {{ $errors->has('ski_kids') ? 'has-error' : ''}}">
            <label for="ski_kids" class="control-label">{{ __('entities.ski_kids') }}</label>
            <input class="form-control" name="ski_kids" type="number" id="ski_kids"
                   value="{{ isset($lesson->ski_kids) ? $lesson->ski_kids : ''}}">
            {!! $errors->first('ski_kids', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="col">
        <div class="form-group {{ $errors->has('snowboard_kids') ? 'has-error' : ''}}">
            <label for="snowboard_kids" class="control-label">{{ __('entities.snowboard_kids') }}</label>
            <input class="form-control" name="snowboard_kids" type="number" id="snowboard_kids"
                   value="{{ isset($lesson->snowboard_kids) ? $lesson->snowboard_kids : ''}}">
            {!! $errors->first('snowboard_kids', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="col">
        <div class="form-group {{ $errors->has('other_kids') ? 'has-error' : ''}}">
            <label for="other_kids" class="control-label">{{ __('entities.other_kids') }}</label>
            <input class="form-control" name="other_kids" type="number" id="other_kids"
                   value="{{ isset($lesson->other_kids) ? $lesson->other_kids : ''}}">
            {!! $errors->first('other_kids', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="form-group {{ $errors->has('ski_adults') ? 'has-error' : ''}}">
            <label for="ski_adults" class="control-label">{{ __('entities.ski_adults') }}</label>
            <input class="form-control" name="ski_adults" type="number" id="ski_adults"
                   value="{{ isset($lesson->ski_adults) ? $lesson->ski_adults : ''}}">
            {!! $errors->first('ski_adults', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="col">
        <div class="form-group {{ $errors->has('snowboard_adults') ? 'has-error' : ''}}">
            <label for="snowboard_adults" class="control-label">{{ __('entities.snowboard_adults') }}</label>
            <input class="form-control" name="snowboard_adults" type="number" id="snowboard_adults"
                   value="{{ isset($lesson->snowboard_adults) ? $lesson->snowboard_adults : ''}}">
            {!! $errors->first('snowboard_adults', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="col">
        <div class="form-group {{ $errors->has('other_adults') ? 'has-error' : ''}}">
            <label for="other_adults" class="control-label">{{ __('entities.other_adults') }}</label>
            <input class="form-control" name="other_adults" type="number" id="other_adults"
                   value="{{ isset($lesson->other_adults) ? $lesson->other_adults : ''}}">
            {!! $errors->first('other_adults', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>
<h3 class="mt-4">{{ __('content.private_lessons') }}</h3>
<div class="row">
    <div class="col-4">
        <div class="form-group {{ $errors->has('other_private') ? 'has-error' : ''}}">
            <label for="other_private" class="control-label">{{ __('entities.other_private') }}</label>
            <input class="form-control" name="other_private" type="number" id="other_private"
                   value="{{ isset($lesson->other_private) ? $lesson->other_private : ''}}">
            {!! $errors->first('other_private', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>
<h3 class="mt-4">{{ __('content.turnover') }}</h3>
<div class="row">
    <div class="col">
        <div class="form-group {{ $errors->has('turnover_private') ? 'has-error' : ''}}">
            <label for="turnover_private" class="control-label">{{ __('entities.turnover_private') }}</label>
            <input class="form-control" name="turnover_private" type="number" id="turnover_private"
                   value="{{ isset($lesson->turnover_private) ? $lesson->turnover_private : ''}}">
            {!! $errors->first('turnover_private', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="col">
        <div class="form-group {{ $errors->has('turnover_group') ? 'has-error' : ''}}">
            <label for="turnover_group" class="control-label">{{ __('entities.turnover_group') }}</label>
            <input class="form-control" name="turnover_group" type="number" id="turnover_group"
                   value="{{ isset($lesson->turnover_group) ? $lesson->turnover_group : ''}}">
            {!! $errors->first('turnover_group', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="col">
        <div class="form-group {{ $errors->has('turnover_other') ? 'has-error' : ''}}">
            <label for="turnover_other" class="control-label">{{ __('entities.turnover_other') }}</label>
            <input class="form-control" name="turnover_other" type="number" id="turnover_other"
                   value="{{ isset($lesson->turnover_other) ? $lesson->turnover_other : ''}}">
            {!! $errors->first('turnover_other', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div><h3 class="mt-4">{{ __('content.salary_sum') }}</h3>
<div class="row">
    <div class="col-4">
        <div class="form-group {{ $errors->has('total_salary') ? 'has-error' : ''}}">
            <label for="total_salary" class="control-label">{{ __('entities.total_salary') }}</label>
            <input class="form-control" name="total_salary" type="number" id="total_salary"
                   value="{{ isset($lesson->total_salary) ? $lesson->total_salary : ''}}">
            {!! $errors->first('total_salary', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>
<h3 class="mt-4">{{ __('entities.school') }}</h3>
<div class="form-group {{ $errors->has('user_id') ? 'has-error' : ''}}">
    <label for="user_id" class="control-label"></label>
    {{ Form::select('user_id', $fks['users'], isset($lesson->user_id) ? $lesson->user_id : '', ['class' => 'form-control']) }}
    {!! $errors->first('user_id', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit"
           value="{{ $formMode === 'edit' ? __('entities.update') : __('entities.create') }}">
</div>
