<div class="form-group {{ $errors->has('name_de') ? 'has-error' : ''}}">
    <label for="name_de" class="control-label">{{ 'Name De' }}</label>
    <input class="form-control" name="name_de" type="text" id="name_de" value="{{ isset($niveau->name_de) ? $niveau->name_de : ''}}" required>
    {!! $errors->first('name_de', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('name_fr') ? 'has-error' : ''}}">
    <label for="name_fr" class="control-label">{{ 'Name Fr' }}</label>
    <input class="form-control" name="name_fr" type="text" id="name_fr" value="{{ isset($niveau->name_fr) ? $niveau->name_fr : ''}}" required>
    {!! $errors->first('name_fr', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('name_en') ? 'has-error' : ''}}">
    <label for="name_en" class="control-label">{{ 'Name En' }}</label>
    <input class="form-control" name="name_en" type="text" id="name_en" value="{{ isset($niveau->name_en) ? $niveau->name_en : ''}}" >
    {!! $errors->first('name_en', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
