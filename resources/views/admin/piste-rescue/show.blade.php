@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">PisteRescue {{ $pisterescue->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/piste-rescue') }}" title="{{__('entities.back') }}"><button class="btn btn-outline-primary btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{__('entities.back') }}</button></a>
                        <a href="{{ url('/admin/piste-rescue/' . $pisterescue->id . '/edit') }}" title="{{__('entities.edit') }} PisteRescue"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{__('entities.edit') }}</button></a>

                        <form method="POST" action="{{ url('admin/pisterescue' . '/' . $pisterescue->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-outline-primary btn-sm" title="Delete PisteRescue" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> {{__('entities.delete') }}</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $pisterescue->id }}</td>
                                    </tr>
                                    <tr><th> Name De </th><td> {{ $pisterescue->name_de }} </td></tr><tr><th> Name Fr </th><td> {{ $pisterescue->name_fr }} </td></tr><tr><th> Name En </th><td> {{ $pisterescue->name_en }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
