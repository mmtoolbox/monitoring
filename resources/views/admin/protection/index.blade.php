@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Protection</div>
                    <div class="card-body">
                        <a href="{{ url('/admin/protection/create') }}" class="btn btn-primary btn-sm" title="{{__('entities.add_new') }} Protection">
                            <i class="fa fa-plus" aria-hidden="true"></i> {{__('entities.add_new') }}
                        </a>

                        <form method="GET" action="{{ url('/admin/protection') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="{{__('entities.search') }}..." value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Name De</th><th>Name Fr</th><th>Name En</th><th>{{__('entities.actions') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($protection as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->name_de }}</td><td>{{ $item->name_fr }}</td><td>{{ $item->name_en }}</td>
                                        <td>
                                            <a href="{{ url('/admin/protection/' . $item->id) }}" title="View Protection"><button class="btn btn-outline-primary btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> {{__('entities.view') }}</button></a>
                                            <a href="{{ url('/admin/protection/' . $item->id . '/edit') }}" title="Edit Protection"><button class="btn btn-outline-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{__('entities.edit') }}</button></a>

                                            <form method="POST" action="{{ url('/admin/protection' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-outline-primary btn-sm" title="Delete Protection" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> {{__('entities.delete') }}</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $protection->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
