@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">BodyPart {{ $bodypart->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/body-part') }}" title="{{__('entities.back') }}"><button class="btn btn-outline-primary btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{__('entities.back') }}</button></a>
                        <a href="{{ url('/admin/body-part/' . $bodypart->id . '/edit') }}" title="{{__('entities.edit') }} BodyPart"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{__('entities.edit') }}</button></a>

                        <form method="POST" action="{{ url('admin/bodypart' . '/' . $bodypart->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-outline-primary btn-sm" title="Delete BodyPart" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> {{__('entities.delete') }}</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $bodypart->id }}</td>
                                    </tr>
                                    <tr><th> Name De </th><td> {{ $bodypart->name_de }} </td></tr><tr><th> Name Fr </th><td> {{ $bodypart->name_fr }} </td></tr><tr><th> Name En </th><td> {{ $bodypart->name_en }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
