@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Season {{ $season->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/seasons') }}" title="Back">
                            <button class="btn btn-outline-primary btn-sm"><i class="fa fa-arrow-left"
                                                                              aria-hidden="true"></i> Back
                            </button>
                        </a>
                        <a href="{{ url('/admin/seasons/' . $season->id . '/edit') }}" title="Edit Season">
                            <button class="btn btn-outline-primary btn-sm"><i class="fa fa-pencil-square-o"
                                                                              aria-hidden="true"></i> Edit
                            </button>
                        </a>

                        <form method="POST" action="{{ url('admin/seasons' . '/' . $season->id) }}"
                              accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-outline-primary btn-sm" title="Delete Season"
                                    onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o"
                                                                                             aria-hidden="true"></i>
                                Delete
                            </button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $season->id }}</td>
                                </tr>
                                <tr>
                                    <th> Title</th>
                                    <td> {{ $season->title }} </td>
                                </tr>
                                <tr>
                                    <th> # {{ __('entities.lesson') }}</th>
                                    <td> {{ $season->lessons_count }} </td>
                                </tr>
                                <tr>
                                    <th> Start Date</th>
                                    <td> {{ $season->start_date }} </td>
                                </tr>
                                <tr>
                                    <th> End Date</th>
                                    <td> {{ $season->end_date }} </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
