@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Accident {{ $accident->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/accident') }}" title="{{ __('entities.back') }}">
                            <button class="btn btn-outline-primary btn-sm"><i class="fa fa-arrow-left"
                                                                              aria-hidden="true"></i> {{ __('entities.back') }}
                            </button>
                        </a>
                        <!--
                        <a href="{{ url('/user/accident/' . $accident->id . '/edit') }}"
                           title="{{__('entities.edit') }} Accident">
                            <button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"
                                                                      aria-hidden="true"></i> {{__('entities.edit') }}
                            </button>
                        </a>

                        <form method="POST" action="{{ url('user/accident' . '/' . $accident->id) }}"
                              accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-outline-primary btn-sm" title="Delete Accident"
                                    onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o"
                                                                                             aria-hidden="true"></i> {{__('entities.delete') }}
                            </button>
                        </form>

                        -->
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <th> {{ __('entities.accident_title') }}</th>
                                    <td> {{ $accident->title }} </td>
                                </tr>
                                <tr>
                                    <th> {{ __('entities.accident_date') }}</th>
                                    <td> {{ \Carbon\Carbon::parse($accident->date)->format('d.m.Y') }} </td>
                                </tr>
                                <tr>
                                    <th> {{ __('entities.accident_place') }}</th>
                                    <td> {{ $accident->accident_place['name_' . Config::get('app.locale') ]}} </td>
                                </tr>
                                <tr>
                                    <th> {{ __('entities.accident_activity') }}</th>
                                    <td> {{ $accident->accident_activity['name_' . Config::get('app.locale') ]}} </td>
                                </tr>
                                <tr>
                                    <th> {{ __('entities.accident_teacher') }}</th>
                                    <td> {{ $accident->teacher }} </td>
                                </tr>
                                <tr>
                                    <th> {{ __('entities.accident_weather') }}</th>
                                    <td> {{ $accident->weather['name_' . Config::get('app.locale') ]}} </td>
                                </tr>
                                <tr>
                                    <th> {{ __('entities.accident_snow_conditions') }}</th>
                                    <td> {{ $accident->snow_condition['name_' . Config::get('app.locale') ]}} </td>
                                </tr>
                                <tr>
                                    <th> {{ __('entities.accident_type') }}</th>
                                    <td> {{ $accident->accident_type['name_' . Config::get('app.locale') ]}} </td>
                                </tr>
                                <tr>
                                    <th> {{ __('entities.accident_witnesses') }}</th>
                                    <td> {{ $accident->witnesses }} </td>
                                </tr>
                                <tr>
                                    <th> {{ __('entities.accident_piste_rescue') }}</th>
                                    <td> {{ $accident->piste_rescue['name_' . Config::get('app.locale') ] }} </td>
                                </tr>
                                <tr>
                                    <th> {{ __('entities.accident_description') }}</th>
                                    <td> {{ $accident->description }} </td>
                                </tr>
                                <tr>
                                    <th> {{ __('entities.accident_gravity') }}</th>
                                    <td> {{ $accident->accident_gravity['name_' . Config::get('app.locale') ] }} </td>
                                </tr>
                                <tr>
                                    <th> {{ __('entities.accident_protection') }}</th>
                                    <td> {{ $accident->protection['name_' . Config::get('app.locale') ] }} </td>
                                </tr>
                                <tr>
                                    <th> {{ __('entities.accident_injured_person') }}</th>
                                    <td> {{ $accident->injured_person_str }} </td>
                                </tr>
                                <tr>
                                    <th> {{ __('entities.accident_age_category') }}</th>
                                    <td> {{ $accident->age_category['name'] }} </td>
                                </tr>
                                <tr>
                                    <th> {{ __('entities.accident_sex') }}</th>
                                    <td> {{ $accident->sex }} </td>
                                </tr>
                                <tr>
                                    <th> {{ __('entities.accident_discipline') }}</th>
                                    <td> {{ $accident->discipline['name_' . Config::get('app.locale') ] }} </td>
                                </tr>
                                <tr>
                                    <th> {{ __('entities.accident_niveau') }}</th>
                                    <td> {{ $accident->niveau['name_' . Config::get('app.locale') ] }} </td>
                                </tr>
                                <tr>
                                    <th> {{ __('entities.accident_injured_lastname') }}</th>
                                    <td> {{ $accident->injured_lastname }} </td>
                                </tr>
                                <tr>
                                    <th> {{ __('entities.accident_injured_firstname') }}</th>
                                    <td> {{ $accident->injured_firstname }} </td>
                                </tr>
                                <tr>
                                    <th> {{ __('entities.accident_injured_birthdate') }}</th>
                                    <td> {{ $accident->injured_birthdate }} </td>
                                </tr>
                                <tr>
                                    <th> {{ __('entities.accident_injured_street') }}</th>
                                    <td> {{ $accident->injured_street }} </td>
                                </tr>
                                <tr>
                                    <th> {{ __('entities.accident_injured_plz') }}</th>
                                    <td> {{ $accident->injured_plz }} </td>
                                </tr>
                                <tr>
                                    <th> {{ __('entities.accident_injured_town') }}</th>
                                    <td> {{ $accident->injured_town }} </td>
                                </tr>
                                <tr>
                                    <th> {{ __('entities.accident_injured_country') }}</th>
                                    <td> {{ $accident->injured_country }} </td>
                                </tr>
                                <tr>
                                    <th> {{ __('entities.accident_injured_holiday_street') }}</th>
                                    <td> {{ $accident->injured_holiday_street }} </td>
                                </tr>
                                <tr>
                                    <th> {{ __('entities.accident_injured_holiday_plz') }}</th>
                                    <td> {{ $accident->injured_holiday_plz }} </td>
                                </tr>
                                <tr>
                                    <th> {{ __('entities.accident_injured_holiday_town') }}</th>
                                    <td> {{ $accident->injured_holiday_town }} </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
