@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">{{__('entities.accidents') }}</div>
                    <div class="card-body">
                    <!--
                        <a href="{{ url('/user/accident/create') }}" class="btn btn-primary btn-sm"
                           title="{{__('entities.add_new') }} Accident">
                            <i class="fa fa-plus" aria-hidden="true"></i> {{__('entities.add_new') }}
                        </a>-->

                        <a href="{{ url('/admin/accident/export') }}" class="btn btn-secondary btn-sm"
                           title="{{__('entities.export') }}">
                            <i class="fa fa-file-excel-o" aria-hidden="true"></i> {{__('entities.export') }}
                        </a>

                        <form method="GET" action="{{ url('/admin/accident') }}" accept-charset="UTF-8"
                              class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control form-control-sm" name="search"
                                       placeholder="{{__('entities.search') }}..." value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary btn-sm" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{__('entities.school')}}</th>
                                    <th>{{__('entities.accident_title')}}</th>
                                    <th>{{__('entities.accident_date')}}</th>
                                    <th>{{__('entities.actions') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($accident as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->user->name }}</td>
                                        <td>{{ $item->title }}</td>
                                        <td>{{ \Carbon\Carbon::parse($item->date)->format('d.m.Y') }}</td>
                                        <td>
                                            <a href="{{ url('/admin/accident/' . $item->id) }}" title="View Accident">
                                                <button class="btn btn-outline-primary btn-sm"><i class="fa fa-eye"
                                                                                                  aria-hidden="true"></i> {{__('entities.view') }}
                                                </button>
                                            </a>
                                        <!--
                                            <a href="{{ url('/user/accident/' . $item->id . '/edit') }}"
                                               title="Edit Accident">
                                                <button class="btn btn-outline-primary btn-sm"><i
                                                        class="fa fa-pencil-square-o"
                                                        aria-hidden="true"></i> {{__('entities.edit') }}</button>
                                            </a>

                                            <form method="POST" action="{{ url('/user/accident' . '/' . $item->id) }}"
                                                  accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                        {{ csrf_field() }}
                                            <button type="submit" class="btn btn-outline-primary btn-sm"
                                                    title="Delete Accident"
                                                    onclick="return confirm(&quot;Confirm delete?&quot;)"><i
                                                        class="fa fa-trash-o"
                                                        aria-hidden="true"></i> {{__('entities.delete') }}</button>
                                            </form>
                                            -->
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div
                                class="pagination-wrapper"> {!! $accident->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
