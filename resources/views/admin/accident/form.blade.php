<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    <label for="title" class="control-label">{{ 'Title' }}</label>
    <input class="form-control" name="title" type="text" id="title" value="{{ isset($accident->title) ? $accident->title : ''}}" required>
    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('date') ? 'has-error' : ''}}">
    <label for="date" class="control-label">{{ 'Date' }}</label>
    <input class="form-control" name="date" type="date" id="date" value="{{ isset($accident->date) ? $accident->date : ''}}" required>
    {!! $errors->first('date', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('accident_place_id') ? 'has-error' : ''}}">
    <label for="accident_place_id" class="control-label">{{ 'Accident Place Id' }}</label>
    {{ Form::select('accident_place_id', $fks['accident_places'], isset($accident->accident_place_id) ? $accident->accident_place_id : '', ['class' => 'form-control']) }}
    {!! $errors->first('accident_place_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('accident_activity_id') ? 'has-error' : ''}}">
    <label for="accident_activity_id" class="control-label">{{ 'Accident Activity Id' }}</label>
    {{ Form::select('accident_activity_id', $fks['accident_activities'], isset($accident->accident_activity_id) ? $accident->accident_activity_id : '', ['class' => 'form-control']) }}
    {!! $errors->first('accident_activity_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('teacher') ? 'has-error' : ''}}">
    <label for="teacher" class="control-label">{{ 'Teacher' }}</label>
    <input class="form-control" name="teacher" type="text" id="teacher" value="{{ isset($accident->teacher) ? $accident->teacher : ''}}" >
    {!! $errors->first('teacher', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('weather_id') ? 'has-error' : ''}}">
    <label for="weather_id" class="control-label">{{ 'Weather Id' }}</label>
    {{ Form::select('weather_id', $fks['weather'], isset($accident->weather_id) ? $accident->weather_id : '', ['class' => 'form-control']) }}
    {!! $errors->first('weather_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('snow_conditions_id') ? 'has-error' : ''}}">
    <label for="snow_conditions_id" class="control-label">{{ 'Snow Conditions Id' }}</label>
    {{ Form::select('snow_conditions_id', $fks['snow_conditions'], isset($accident->snow_conditions_id) ? $accident->snow_conditions_id : '', ['class' => 'form-control']) }}
    {!! $errors->first('snow_conditions_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('accident_type_id') ? 'has-error' : ''}}">
    <label for="accident_type_id" class="control-label">{{ 'Accident Type Id' }}</label>
    {{ Form::select('accident_type_id', $fks['accident_types'], isset($accident->accident_type_id) ? $accident->accident_type_id : '', ['class' => 'form-control']) }}
    {!! $errors->first('accident_type_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('witnesses') ? 'has-error' : ''}}">
    <label for="witnesses" class="control-label">{{ 'Witnesses' }}</label>
    <input class="form-control" name="witnesses" type="text" id="witnesses" value="{{ isset($accident->witnesses) ? $accident->witnesses : ''}}" >
    {!! $errors->first('witnesses', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('piste_rescue_id') ? 'has-error' : ''}}">
    <label for="piste_rescue_id" class="control-label">{{ 'Piste Rescue Id' }}</label>
    {{ Form::select('piste_rescue_id', $fks['piste_rescues'], isset($accident->piste_rescue_id) ? $accident->piste_rescue_id : '', ['class' => 'form-control']) }}
    {!! $errors->first('piste_rescue_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    <label for="description" class="control-label">{{ 'Description' }}</label>
    <textarea class="form-control" rows="5" name="description" type="textarea" id="description" >{{ isset($accident->description) ? $accident->description : ''}}</textarea>
    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('accident_gravity_id') ? 'has-error' : ''}}">
    <label for="accident_gravity_id" class="control-label">{{ 'Accident Gravity Id' }}</label>
    {{ Form::select('accident_gravity_id', $fks['accident_gravities'], isset($accident->accident_gravity_id) ? $accident->accident_gravity_id : '', ['class' => 'form-control']) }}
    {!! $errors->first('accident_gravity_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('protection_id') ? 'has-error' : ''}}">
    <label for="protection_id" class="control-label">{{ 'Protection Id' }}</label>
    {{ Form::select('protection_id', $fks['protections'], isset($accident->protection_id) ? $accident->protection_id : '', ['class' => 'form-control']) }}
    {!! $errors->first('protection_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('body_part_ids[]') ? 'has-error' : ''}}">
    <label for="body_part_ids[]" class="control-label">{{ 'BodyPart Ids' }}</label>
    {{ Form::select('body_part_ids[]', $fks['body_parts'], isset($accident->body_parts) ? $accident->body_parts->pluck('id') : '', ['class' => 'form-control', 'multiple' => '']) }}
    {!! $errors->first('body_part_ids[]', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('injury_type_ids[]') ? 'has-error' : ''}}">
    <label for="injury_type_ids" class="control-label">{{ 'injury_type Ids' }}</label>
    {{ Form::select('injury_type_ids[]', $fks['injury_types'], isset($accident->injury_types) ? $accident->injury_types->pluck('id') : '', ['class' => 'form-control', 'multiple' => '']) }}
    {!! $errors->first('injury_type_ids[]', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('injured_person') ? 'has-error' : ''}}">
    <label for="injured_person" class="control-label">{{ 'Injured Person' }}</label>
    <select name="injured_person" class="form-control" id="injured_person" required>
    @foreach (json_decode('{"guest":"Guest","teacher":"Teacher"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($accident->injured_person) && $accident->injured_person == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
    {!! $errors->first('injured_person', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('age_category_id') ? 'has-error' : ''}}">
    <label for="age_category_id" class="control-label">{{ 'Age Category Id' }}</label>
    {{ Form::select('age_category_id', $fks['age_categories'], isset($accident->age_category_id) ? $accident->age_category_id : '', ['class' => 'form-control']) }}
    {!! $errors->first('age_category_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('sex') ? 'has-error' : ''}}">
    <label for="sex" class="control-label">{{ 'Sex' }}</label>
    <select name="sex" class="form-control" id="sex" required>
    @foreach (json_decode('{"m":"Male","f":"Female"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($accident->sex) && $accident->sex == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
    {!! $errors->first('sex', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('discipline_id') ? 'has-error' : ''}}">
    <label for="discipline_id" class="control-label">{{ 'Discipline Id' }}</label>
    {{ Form::select('discipline_id', $fks['disciplines'], isset($accident->discipline_id) ? $accident->discipline_id : '', ['class' => 'form-control']) }}
    {!! $errors->first('discipline_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('niveau_id') ? 'has-error' : ''}}">
    <label for="niveau_id" class="control-label">{{ 'Niveau Id' }}</label>
    {{ Form::select('niveau_id', $fks['niveaux'], isset($accident->niveau_id) ? $accident->niveau_id : '', ['class' => 'form-control']) }}
    {!! $errors->first('niveau_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('injured_lastname') ? 'has-error' : ''}}">
    <label for="injured_lastname" class="control-label">{{ 'Injured Lastname' }}</label>
    <input class="form-control" name="injured_lastname" type="text" id="injured_lastname" value="{{ isset($accident->injured_lastname) ? $accident->injured_lastname : ''}}" >
    {!! $errors->first('injured_lastname', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('injured_firstname') ? 'has-error' : ''}}">
    <label for="injured_firstname" class="control-label">{{ 'Injured Firstname' }}</label>
    <input class="form-control" name="injured_firstname" type="text" id="injured_firstname" value="{{ isset($accident->injured_firstname) ? $accident->injured_firstname : ''}}" >
    {!! $errors->first('injured_firstname', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('injured_birthdate') ? 'has-error' : ''}}">
    <label for="injured_birthdate" class="control-label">{{ 'Injured Birthdate' }}</label>
    <input class="form-control" name="injured_birthdate" type="date" id="injured_birthdate" value="{{ isset($accident->injured_birthdate) ? $accident->injured_birthdate : ''}}" >
    {!! $errors->first('injured_birthdate', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('injured_street') ? 'has-error' : ''}}">
    <label for="injured_street" class="control-label">{{ 'Injured Street' }}</label>
    <input class="form-control" name="injured_street" type="text" id="injured_street" value="{{ isset($accident->injured_street) ? $accident->injured_street : ''}}" >
    {!! $errors->first('injured_street', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('injured_plz') ? 'has-error' : ''}}">
    <label for="injured_plz" class="control-label">{{ 'Injured Plz' }}</label>
    <input class="form-control" name="injured_plz" type="number" id="injured_plz" value="{{ isset($accident->injured_plz) ? $accident->injured_plz : ''}}" >
    {!! $errors->first('injured_plz', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('injured_town') ? 'has-error' : ''}}">
    <label for="injured_town" class="control-label">{{ 'Injured Town' }}</label>
    <input class="form-control" name="injured_town" type="text" id="injured_town" value="{{ isset($accident->injured_town) ? $accident->injured_town : ''}}" >
    {!! $errors->first('injured_town', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('injured_country') ? 'has-error' : ''}}">
    <label for="injured_country" class="control-label">{{ 'Injured Country' }}</label>
    <input class="form-control" name="injured_country" type="text" id="injured_country" value="{{ isset($accident->injured_country) ? $accident->injured_country : ''}}" >
    {!! $errors->first('injured_country', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('injured_holiday_street') ? 'has-error' : ''}}">
    <label for="injured_holiday_street" class="control-label">{{ 'Injured Holiday Street' }}</label>
    <input class="form-control" name="injured_holiday_street" type="text" id="injured_holiday_street" value="{{ isset($accident->injured_holiday_street) ? $accident->injured_holiday_street : ''}}" >
    {!! $errors->first('injured_holiday_street', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('injured_holiday_plz') ? 'has-error' : ''}}">
    <label for="injured_holiday_plz" class="control-label">{{ 'Injured Holiday Plz' }}</label>
    <input class="form-control" name="injured_holiday_plz" type="number" id="injured_holiday_plz" value="{{ isset($accident->injured_holiday_plz) ? $accident->injured_holiday_plz : ''}}" >
    {!! $errors->first('injured_holiday_plz', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('injured_holiday_town') ? 'has-error' : ''}}">
    <label for="injured_holiday_town" class="control-label">{{ 'Injured Holiday Town' }}</label>
    <input class="form-control" name="injured_holiday_town" type="text" id="injured_holiday_town" value="{{ isset($accident->injured_holiday_town) ? $accident->injured_holiday_town : ''}}" >
    {!! $errors->first('injured_holiday_town', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? __('entities.update') : __('entities.create') }}">
</div>
