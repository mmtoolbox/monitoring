<div class="form-group {{ $errors->has('id') ? 'has-error' : ''}}">
    <label for="id" class="control-label">{{ 'Id' }}</label>
    @if ($formMode === 'edit')
        <input class="form-control" name="id" type="number" id="id" value="{{ isset($user->id) ? $user->id : ''}}" readonly>
    @else
        <input class="form-control" name="id" type="number" id="id" value="{{ isset($user->id) ? $user->id : ''}}">
    @endif
    {!! $errors->first('plz', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'Name' }}</label>
    <input class="form-control" name="name" type="text" id="name" value="{{ isset($user->name) ? $user->name : ''}}"
           required>
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'E-Mail' }}</label>
    <input class="form-control" name="email" type="" id="email" value="{{ isset($user->email) ? $user->email : ''}}"
               required>
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
    <label for="password" class="control-label">{{ 'Password (leer lassen, wenn keine Änderung)' }}</label>
    <input class="form-control" name="password" type="password" id="password" value="">
    {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('company') ? 'has-error' : ''}}">
    <label for="company" class="control-label">{{ 'Company' }}</label>
    <input class="form-control" name="company" type="text" id="company"
           value="{{ isset($user->company) ? $user->company : ''}}" required>
    {!! $errors->first('company', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('plz') ? 'has-error' : ''}}">
    <label for="plz" class="control-label">{{ 'Plz' }}</label>
    <input class="form-control" name="plz" type="number" id="plz" value="{{ isset($user->plz) ? $user->plz : ''}}" required>
    {!! $errors->first('plz', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('town') ? 'has-error' : ''}}">
    <label for="town" class="control-label">{{ 'Town' }}</label>
    <input class="form-control" name="town" type="text" id="town" value="{{ isset($user->town) ? $user->town : ''}}" required>
    {!! $errors->first('town', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('language_id') ? 'has-error' : ''}}">
    <label for="language_id" class="control-label">{{ 'Language Id' }}</label>
    <select name="language_id" class="form-control" id="language_id">
        @foreach (json_decode('{"1":"DE","2":"FR"}', true) as $optionKey => $optionValue)
            <option
                value="{{ $optionKey }}" {{ (isset($user->language_id) && $user->language_id == $optionKey) ? 'selected' : ''}}>
                {{ $optionValue }}
            </option>
        @endforeach
    </select>
    {!! $errors->first('language_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('region_id') ? 'has-error' : ''}}">
    <label for="region_id" class="control-label">{{ 'Region Id' }}</label>
    <select name="region_id" class="form-control" id="region_id">
        @foreach (json_decode('{"1":"Bern","2":"Graub\u00fcnden","3":"Ostschweiz","4":"Tessin","5":"Waadt","6":"Wallis","7":"Westschweiz","8":"Zentralschweiz"}', true) as $optionKey => $optionValue)
            <option
                value="{{ $optionKey }}" {{ (isset($user->region_id) && $user->region_id == $optionKey) ? 'selected' : ''}}>
                {{ $optionValue }}
            </option>
        @endforeach
    </select>
    {!! $errors->first('region_id', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit"
           value="{{ $formMode === 'edit' ? __('entities.update') : __('entities.create') }}">
</div>
