@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">{{ __('entities.school') }} {{ $user->name }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/user') }}" title="{{__('entities.back') }}">
                            <button class="btn btn-outline-primary btn-sm"><i class="fa fa-arrow-left"
                                                                              aria-hidden="true"></i> {{__('entities.back') }}
                            </button>
                        </a>
                        <a href="{{ url('/admin/user/' . $user->id . '/edit') }}" title="{{__('entities.edit') }} User">
                            <button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"
                                                                      aria-hidden="true"></i> {{__('entities.edit') }}
                            </button>
                        </a>

                        <form method="POST" action="{{ url('admin/user' . '/' . $user->id) }}" accept-charset="UTF-8"
                              style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-outline-primary btn-sm" title="Delete User"
                                    onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o"
                                                                                             aria-hidden="true"></i> {{__('entities.delete') }}
                            </button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $user->id }}</td>
                                </tr>
                                <tr>
                                    <th> Name</th>
                                    <td> {{ $user->name }} </td>
                                </tr>
                                <tr>
                                    <th> Company</th>
                                    <td> {{ $user->company }} </td>
                                </tr>
                                <tr>
                                    <th> E-Mail</th>
                                    <td> {{ $user->email }} </td>
                                </tr>
                                <tr>
                                    <th> PLZ/Ort</th>
                                    <td> {{ $user->plz }} {{ $user->town }}</td>
                                </tr>
                                <tr>
                                    <th> Sprache</th>
                                    <td> {{ [1 => 'DE', 2 => 'FR'] [$user->language_id] }}</td>
                                </tr>
                                <tr>
                                    <th> Region</th>
                                    <td> {{ json_decode('{"1":"Bern","2":"Graub\u00fcnden","3":"Ostschweiz","4":"Tessin","5":"Waadt","6":"Wallis","7":"Westschweiz","8":"Zentralschweiz"}', true)[$user->region_id] }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
