@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">AccidentPlace {{ $accidentplace->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/accident-place') }}" title="Back"><button class="btn btn-outline-primary  btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/admin/accident-place/' . $accidentplace->id . '/edit') }}" title="Edit AccidentPlace"><button class="btn btn-outline-primary  btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('admin/accidentplace' . '/' . $accidentplace->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-outline-primary btn-sm" title="Delete AccidentPlace" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $accidentplace->id }}</td>
                                    </tr>
                                    <tr><th> Name De </th><td> {{ $accidentplace->name_de }} </td></tr><tr><th> Name Fr </th><td> {{ $accidentplace->name_fr }} </td></tr><tr><th> Name En </th><td> {{ $accidentplace->name_en }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
