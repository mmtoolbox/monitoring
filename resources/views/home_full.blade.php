@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        @include('includes.sidebar')

        <div class="col-md-9">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <p>{{ __('content.home_text') }}</p>
                    <p>{{ __('content.contact') }}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
