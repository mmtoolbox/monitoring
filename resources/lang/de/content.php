<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'group_lessons' => 'Gruppenunterricht',
    'private_lessons' => 'Privatunterricht',
    'accident_report' => 'Unfallreport',
    'inured_person_info' => 'Informationen über die verunfallte Person',
    'inured_person_holiday_address' => 'Ferienadresse in der Schweiz',
    'accident_info' => 'Informationen zum Unfall',
    'venue_date' => 'Ort, Datum',
    'signature_prof' => 'Unterschrift Lehrer',
    'turnover' => 'Gesamtumsatz',
    'salary_sum' => 'Lohnsumme',
    'total' => 'Total',

    'home_text' => 'Diese Webseite dient Ihnen zur einfachen Erfassung einiger Kennzahlen. Diese werden in anonymisierter Form von Swiss Snowsports zur Öffentlichkeitskommunikation verwendet. Bis anhin werden von den Medien v.a. Übernachtungs- und Bergbahnkennzahlen kommuniziert. Da jedoch auch der Schneesport eine wichtige Kenngrösse ist, die es zu kommunizieren gilt, sind wir auf eine aktuelle Erfassung Ihrer Daten angewiesen.

Ebenso können Sie diese Webseite aber auch für Ihr eigenes Unternehmenscontrolling verwenden. So erhalten Sie nach der Erfassung nützliche und übersichtlich gestaltete Auswertungen. Besten Dank für Ihre Teilnahme.',
    'contact' => 'SWISS SNOWSPORTS
Hühnerhubelstrasse 95
CH-3123 Belp
Telefon: +41 (0)31 810 41 11
Fax: +41 (0)31 810 41 12
E-Mail: <a href="mailto:info@snowsports.ch">info@snowsports.ch</a>
<a href="https://www.snowsports.ch" target="_blank">www.snowsports.ch</a>',
    'home' => 'Home'
];
