<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'lesson' => 'Kennzahlen',
    'season' => 'Saison',
    'weather' => 'Wetter',
    'niveau' => 'Niveau',

    'ski_kids' => 'Kinder Ski',
    'snowboard_kids' => 'Kinder Snowboard',
    'other_kids' => 'Kinder Andere',
    'ski_adults' => 'Erwachsene Ski',
    'snowboard_adults' => 'Erwachsene Snowboard',
    'other_adults' => 'Erwachsene Andere',
    'other_private' => 'Total',
    'turnover_private' => 'Privat (Total)',
    'turnover_group' => 'Gruppe (Ski/Snowboard sowie Kind/Erwachsene)',
    'turnover_other' => 'Andere (Kind/Erwachsene)',
    'total_salary' => 'Total',
    'error_season_duplicate' => 'Für die ausgewählte Saison besteht bereits ein Eintrag. Bitte bearbeite diesen.',
    'create_new_lesson' => 'Neuen Eintrag hinzufügen',
    'total_lessons_kids' => 'Total',
    'total_lessons_adults' => 'Total',
    'lessons_example_text' => '
<div class="alert alert-secondary" role="alert">
    <h3>Berechnungsbeispiele</h3>
    <table class="table table-sm">
    <tr>
    <td>Gruppenunterricht Kinder Ski</td>
    <td>7 Kinder * 4 h * 5 Tage</td>
    <td>= 140 Stunden</td>
</tr><tr>
<td> </td>
<td>4 Kinder * 3 h</td>
<td>= 12 Stunden</td>
</tr>
    <tr>
    <td>Gruppenunterricht Erwachsene Snowboard</td>
    <td>5 Erwachsene * 2 h * 3 Tage</td>
    <td>= 30 Stunden</td>
</tr>
    <tr>
    <td>Privatunterricht</td>
    <td>1 Erwachsener * 2 h</td>
    <td>= 2 Stunden</td>
</tr>
</table></div>  
    ',

    'accident' => 'Unfall',
    'accidents' => 'Unfälle',
    'accidents_overview' => 'Übersicht Unfälle',
    'accident_title' => 'Titel/Schlagwort',
    'accident_date' => 'Datum',
    'accident_place' => 'Unfallort',
    'accident_activity' => 'Tätigkeit',
    'accident_teacher' => 'Name & Vorname Lehrperson',
    'accident_weather' => 'Wetter',
    'accident_snow_conditions' => 'Schneeverhältnis',
    'accident_type' => 'Unfallart',
    'accident_witnesses' => 'Zeugen',
    'accident_piste_rescue' => 'Pistenrettungsdienst involviert',
    'accident_description' => 'Beschreibung Unfallhergang',
    'accident_gravity' => 'Unfallschwere',
    'accident_protection' => 'Getragene Schutzausrüstung',
    'accident_injured_person' => 'Verunfallter',
    'accident_body_part' => 'Verletztes Körperteil',
    'accident_injury_type' => 'Verletzungsart',
    'accident_age_category' => 'Alter',
    'accident_sex' => 'Geschlecht',
    'accident_discipline' => 'Sportart',
    'accident_niveau' => 'Niveau',
    'accident_injured_lastname' => 'Nachname',
    'accident_injured_firstname' => 'Vorname',
    'accident_injured_birthdate' => 'Geburtsdatum',
    'accident_injured_street' => 'Strasse',
    'accident_injured_plz' => 'PLZ',
    'accident_injured_town' => 'Wohnort',
    'accident_injured_country' => 'Land',
    'accident_injured_holiday_street' => 'Strasse',
    'accident_injured_holiday_plz' => 'PLZ',
    'accident_injured_holiday_town' => 'Ort',
    'create_new_accident' => 'Neuen Unfall erfassen',
    'guest' => 'Gast',
    'teacher' => 'Lehrperson',

    'add_new' => 'Hinzufügen',
    'actions' => 'Aktionen',
    'view' => 'Ansehen',
    'edit' => 'Bearbeiten',
    'delete' => 'Löschen',
    'search' => 'Suchen',
    'back' => 'Zurück',
    'create' => 'Erfassen',
    'update' => 'Aktualisieren',
    'export' => 'Exportieren',

    'snow-condition' => 'Schneeverhältnisse',
    'accident-type' => 'Unfallart',
    'piste-rescue' => 'Pistenrettung',
    'accident-gravity' => 'Schwere des Unfalls',
    'protection' => 'Getragene Schutzausrüstung',
    'injury-type' => 'Verletzungstyp',
    'body-part' => 'Körperteil',
    'discipline' => 'Sportart',
    'age-category' => 'Alterskategorie',
    'accident-activity' => 'Tätigkeit',

    'school' => 'Schneesportschule',
    'schools' => 'Schneesportschulen'

];
