<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'lesson' => 'Lesson',
    'ski_kids' => '',
    'snowboard_kids' => '',
    'other_kids' => '',
    'ski_adults' => '',
    'snowboard_adults' => '',
    'other_adults' => '',
    'other_private' => '',
    'turnover_private' => '',
    'turnover_group' => '',
    'turnover_other' => '',
    'total_salary' => '',

    'season' => 'Season',

    'accident' => 'Accident',
    'accidents' => 'Accidents',


    'add_new' => 'Add New',
    'actions' => 'Actions',
    'view' => 'View',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'search' => 'Search'

];
