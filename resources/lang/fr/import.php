<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'import' => 'Kassensystem-Import',
    'file_to_import' => 'CSV-Datei für den Import',
    'import_csv' => 'CSV-Datei importieren',
    'file_imported' => 'Datei importiert',
    'error_in_line_not_imported' => 'Zeile :line konnte nicht importiert werden:<br>:exception',
    'line_imported' => 'Zeile :line importiert',
    'data' => 'Daten',
    'error_in_format' => 'Die Datei ist fehlerhaft formatiert. (Bsp. zu viele oder zu wenige Spalten)'
];
