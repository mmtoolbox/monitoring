<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'group_lessons' => 'Gruppenunterricht',
    'private_lessons' => 'Privatunterricht',
    'accident_report' => 'Unfallreport',
    'inured_person_info' => 'Informationen über die verunfallte Person',
    'inured_person_holiday_address' => 'Ferienadresse in der Schweiz',
    'accident_info' => 'Informationen zum Unfall',
    'venue_date' => 'Ort, Datum',
    'signature_prof' => 'Unterschrift Lehrer',
    'turnover' => 'Gesamtumsatz',
    'salary_sum' => 'Lohnsumme',
    'total' => 'Total',

    'home_text' => 'Ce site Internet est uniquement fait pour enregistrer des chiffres clés. Ces chiffres sont confidentiels et sont utilisés par la fédération des sports d’hiver Suisse qui les met à la disposition du public. Jusqu\'à maintenant les seuls chiffres communiqués sont les nuitées et ceux des remontées mécaniques. Étant donné l’importance des sports d’hiver, ces chiffres nous sont précieux et nous sommes dépendants de vos informations.

Votre société pourra contrôler ces chiffres et obtenir des évaluations certainement très utiles sur leur entreprise. Merci beaucoup pour votre participation.',
    'contact' => 'SWISS SNOWSPORTS
Hühnerhubelstrasse 95
CH-3123 Belp
Téléphone: +41 (0)31 810 41 11
Fax: +41 (0)31 810 41 12
E-Mail: <a href="mailto:info@snowsports.ch">info@snowsports.ch</a>
<a href="https://www.snowsports.ch" target="_blank">www.snowsports.ch</a>',
    'home' => 'Home'
];
