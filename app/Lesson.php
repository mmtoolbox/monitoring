<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'lessons';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['ski_kids', 'snowboard_kids', 'other_kids', 'ski_adults', 'snowboard_adults', 'other_adults', 'other_private', 'turnover_private', 'turnover_group', 'turnover_other', 'total_salary', 'season_id', 'user_id'];

    public function getTotalKidsLessonsAttribute(){
        return $this->ski_kids + $this->snowboard_kids + $this->other_kids;
    }

    public function getTotalAdultsLessonsAttribute(){
        return $this->ski_adults + $this->snowboard_adults + $this->other_adults;
    }

    public function getTotalTurnoverAttribute(){
        return $this->turnover_private + $this->turnover_group + $this->turnover_other;
    }

    public function season()
    {
        return $this->belongsTo('App\Season');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
