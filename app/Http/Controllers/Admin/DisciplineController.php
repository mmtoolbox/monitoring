<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Discipline;
use Illuminate\Http\Request;

class DisciplineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $discipline = Discipline::where('name_de', 'LIKE', "%$keyword%")
                ->orWhere('name_fr', 'LIKE', "%$keyword%")
                ->orWhere('name_en', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $discipline = Discipline::latest()->paginate($perPage);
        }

        return view('admin.discipline.index', compact('discipline'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.discipline.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'name_de' => 'required',
			'name_fr' => 'required'
		]);
        $requestData = $request->all();
        
        Discipline::create($requestData);

        return redirect('admin/discipline')->with('flash_message', 'Discipline added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $discipline = Discipline::findOrFail($id);

        return view('admin.discipline.show', compact('discipline'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $discipline = Discipline::findOrFail($id);

        return view('admin.discipline.edit', compact('discipline'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'name_de' => 'required',
			'name_fr' => 'required'
		]);
        $requestData = $request->all();
        
        $discipline = Discipline::findOrFail($id);
        $discipline->update($requestData);

        return redirect('admin/discipline')->with('flash_message', 'Discipline updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Discipline::destroy($id);

        return redirect('admin/discipline')->with('flash_message', 'Discipline deleted!');
    }
}
