<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\AccidentGravity;
use Illuminate\Http\Request;

class AccidentGravityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $accidentgravity = AccidentGravity::where('name_de', 'LIKE', "%$keyword%")
                ->orWhere('name_fr', 'LIKE', "%$keyword%")
                ->orWhere('name_en', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $accidentgravity = AccidentGravity::latest()->paginate($perPage);
        }

        return view('admin.accident-gravity.index', compact('accidentgravity'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.accident-gravity.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'name_de' => 'required',
			'name_fr' => 'required'
		]);
        $requestData = $request->all();
        
        AccidentGravity::create($requestData);

        return redirect('admin/accident-gravity')->with('flash_message', 'AccidentGravity added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $accidentgravity = AccidentGravity::findOrFail($id);

        return view('admin.accident-gravity.show', compact('accidentgravity'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $accidentgravity = AccidentGravity::findOrFail($id);

        return view('admin.accident-gravity.edit', compact('accidentgravity'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'name_de' => 'required',
			'name_fr' => 'required'
		]);
        $requestData = $request->all();
        
        $accidentgravity = AccidentGravity::findOrFail($id);
        $accidentgravity->update($requestData);

        return redirect('admin/accident-gravity')->with('flash_message', 'AccidentGravity updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        AccidentGravity::destroy($id);

        return redirect('admin/accident-gravity')->with('flash_message', 'AccidentGravity deleted!');
    }
}
