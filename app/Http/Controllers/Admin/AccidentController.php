<?php

namespace App\Http\Controllers\Admin;

use App\AccidentActivity;
use App\AccidentGravity;
use App\AccidentPlace;
use App\AccidentType;
use App\AgeCategory;
use App\BodyPart;
use App\Discipline;
use App\Exports\AccidentsExport;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Accident;
use App\InjuryType;
use App\Niveau;
use App\PisteRescue;
use App\Protection;
use App\SnowCondition;
use App\Weather;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class AccidentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $accident = Accident::where('user_id', Auth::user()->id)
                ->where('title', 'LIKE', "%$keyword%")
                        ->orWhere('date', 'LIKE', "%$keyword%")
                        ->orWhere('accident_place_id', 'LIKE', "%$keyword%")
                        ->orWhere('accident_activity_id', 'LIKE', "%$keyword%")
                        ->orWhere('teacher', 'LIKE', "%$keyword%")
                        ->orWhere('weather_id', 'LIKE', "%$keyword%")
                        ->orWhere('snow_conditions_id', 'LIKE', "%$keyword%")
                        ->orWhere('accident_type_id', 'LIKE', "%$keyword%")
                        ->orWhere('witnesses', 'LIKE', "%$keyword%")
                        ->orWhere('piste_rescue_id', 'LIKE', "%$keyword%")
                        ->orWhere('description', 'LIKE', "%$keyword%")
                        ->orWhere('accident_gravity_id', 'LIKE', "%$keyword%")
                        ->orWhere('protection_id', 'LIKE', "%$keyword%")
                        ->orWhere('injured_person', 'LIKE', "%$keyword%")
                        ->orWhere('age_category_id', 'LIKE', "%$keyword%")
                        ->orWhere('sex', 'LIKE', "%$keyword%")
                        ->orWhere('discipline_id', 'LIKE', "%$keyword%")
                        ->orWhere('niveau_id', 'LIKE', "%$keyword%")
                        ->orWhere('injured_lastname', 'LIKE', "%$keyword%")
                        ->orWhere('injured_firstname', 'LIKE', "%$keyword%")
                        ->orWhere('injured_birthdate', 'LIKE', "%$keyword%")
                        ->orWhere('injured_street', 'LIKE', "%$keyword%")
                        ->orWhere('injured_plz', 'LIKE', "%$keyword%")
                        ->orWhere('injured_town', 'LIKE', "%$keyword%")
                        ->orWhere('injured_country', 'LIKE', "%$keyword%")
                        ->orWhere('injured_holiday_street', 'LIKE', "%$keyword%")
                        ->orWhere('injured_holiday_plz', 'LIKE', "%$keyword%")
                        ->orWhere('injured_holiday_town', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $accident = Accident::latest()->paginate($perPage);
        }

        return view('admin.accident.index', compact('accident'));
    }

    /**
     * @return array
     */
    public static function getForeignKeys(){
        return [
            'accident_places' => AccidentPlace::orderBy('id', 'desc')->pluck('name_' . App::getLocale(),'id'),
            'accident_activities' => AccidentActivity::orderBy('id', 'desc')->pluck('name_' . App::getLocale(),'id'),
            'weather' => Weather::orderBy('id', 'desc')->pluck('name_' . App::getLocale(),'id'),
            'snow_conditions' => SnowCondition::orderBy('id', 'desc')->pluck('name_' . App::getLocale(),'id'),
            'accident_types' => AccidentType::orderBy('id', 'desc')->pluck('name_' . App::getLocale(),'id'),
            'piste_rescues' => PisteRescue::orderBy('id', 'desc')->pluck('name_' . App::getLocale(),'id'),
            'accident_gravities' => AccidentGravity::orderBy('id', 'desc')->pluck('name_' . App::getLocale(),'id'),
            'protections' => Protection::orderBy('id', 'desc')->pluck('name_' . App::getLocale(),'id'),
            'age_categories' => AgeCategory::orderBy('id', 'desc')->pluck('name','id'),
            'disciplines' => Discipline::orderBy('id', 'desc')->pluck('name_' . App::getLocale(),'id'),
            'niveaux' => Niveau::orderBy('id', 'desc')->pluck('name_' . App::getLocale(),'id'),
            'body_parts' => BodyPart::orderBy('id', 'desc')->pluck('name_' . App::getLocale(),'id'),
            'injury_types' => InjuryType::orderBy('id', 'desc')->pluck('name_' . App::getLocale(),'id')
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $foreign_keys = self::getForeignKeys();
        return view('admin.accident.create')->with('fks', $foreign_keys);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $this->validate($request, [
			'title' => 'required',
			'date' => 'required',
			'accident_place_id' => 'required',
			'accident_activity_id' => 'required',
			'accident_gravity_id' => 'required',
			'protection_id' => 'required',
			'injured_person' => 'required',
			'age_category_id' => 'required',
			'sex' => 'required',
			'discipline_id' => 'required',
			'niveau_id' => 'required'
		]);
        $requestData = $request->all();

        $accident = Accident::create($requestData);

        // many to many
        $accident->body_parts()->sync($request->post('body_part_ids', []));
        $accident->injury_types()->sync($request->post('injury_type_ids', []));

        return redirect('user/accident')->with('flash_message', 'Accident added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $accident = Accident::findOrFail($id);

        return view('admin.accident.show', compact('accident'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $foreign_keys = self::getForeignKeys();
        $accident = Accident::findOrFail($id);

        return view('admin.accident.edit', compact('accident'))->with('fks', $foreign_keys);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'title' => 'required',
			'date' => 'required',
			'accident_place_id' => 'required',
			'accident_activity_id' => 'required',
			'accident_gravity_id' => 'required',
			'protection_id' => 'required',
			'injured_person' => 'required',
			'age_category_id' => 'required',
			'sex' => 'required',
			'discipline_id' => 'required',
			'niveau_id' => 'required'
		]);
        $requestData = $request->all();

        $accident = Accident::findOrFail($id);
        $accident->update($requestData);

        // many to many
        $accident->body_parts()->sync($request->post('body_part_ids', []));
        $accident->injury_types()->sync($request->post('injury_type_ids', []));

        return redirect('user/accident')->with('flash_message', 'Accident updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Accident::destroy($id);

        return redirect('user/accident')->with('flash_message', 'Accident deleted!');
    }

    public function export(Request $request)
    {

        $file_name = Carbon::now()->toDateString() . '_accidents.xlsx';
        return Excel::download(new AccidentsExport(), $file_name);
    }
}
