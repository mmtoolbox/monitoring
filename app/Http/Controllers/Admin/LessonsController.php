<?php

namespace App\Http\Controllers\Admin;

use App\AccidentPlace;
use App\Exports\LessonsExport;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Lesson;
use App\Season;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class LessonsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $lessons = Lesson::where('ski_kids', 'LIKE', "%$keyword%")
                ->orWhere('snowboard_kids', 'LIKE', "%$keyword%")
                ->orWhere('other_kids', 'LIKE', "%$keyword%")
                ->orWhere('ski_adults', 'LIKE', "%$keyword%")
                ->orWhere('snowboard_adults', 'LIKE', "%$keyword%")
                ->orWhere('other_adults', 'LIKE', "%$keyword%")
                ->orWhere('other_private', 'LIKE', "%$keyword%")
                ->orWhere('turnover_private', 'LIKE', "%$keyword%")
                ->orWhere('turnover_group', 'LIKE', "%$keyword%")
                ->orWhere('turnover_other', 'LIKE', "%$keyword%")
                ->orWhere('total_salary', 'LIKE', "%$keyword%")
                ->orWhere('season_id', 'LIKE', "%$keyword%")
                ->orWhere('user_id', 'LIKE', "%$keyword%")
                ->with('season')
                ->orderBy('user_id', 'asc')
                ->orderBy('season_id', 'desc')
                ->paginate($perPage);
        } else {
            $lessons = Lesson::with('season')
                ->orderBy('user_id', 'asc')
                ->orderBy('season_id', 'desc')
                ->paginate($perPage);
        }


        $seasons = Season::orderBy('id', 'desc')->pluck('title', 'id');

        return view('admin.lessons.index', compact('lessons'))->with('seasons', $seasons);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $seasons = Season::orderBy('id', 'desc')->pluck('title', 'id');
        $fks['users'] = User::orderBy('id', 'desc')->pluck('name','id');

        return view('admin.lessons.create')->with('seasons', $seasons)->with('fks', $fks);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required|unique_with:lessons,season_id',
            'season_id' => 'required'
        ], ['user_id.unique_with' => __('entities.error_season_duplicate')]);
        $requestData = $request->all();

        Lesson::create($requestData);

        return redirect('user/lessons')->with('flash_message', 'Lesson added!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $lesson = Lesson::findOrFail($id);

        return view('admin.lessons.show', compact('lesson'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $lesson = Lesson::findOrFail($id);
        $seasons = Season::orderBy('id', 'desc')->pluck('title', 'id');

        $fks['users'] = User::orderBy('id', 'desc')->pluck('name','id');

        return view('admin.lessons.edit', compact('lesson'))->with('seasons', $seasons)->with('fks', $fks);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'user_id' => 'required'
        ]);
        $requestData = $request->all();

        $lesson = Lesson::findOrFail($id);
        $lesson->update($requestData);

        return redirect('admin/lessons')->with('flash_message', 'Lesson updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Lesson::destroy($id);

        return redirect('user/lessons')->with('flash_message', 'Lesson deleted!');
    }

    public function export(Request $request)
    {
        $season = Season::find($request->get('season_id'));

        $lessons = Lesson::where('season_id', $season->id);
        $file_name = Carbon::now()->toDateString() . '_Saison-' . str_slug($season->title) . '_lessons.xlsx';

        return (new LessonsExport())->forSeason($season->id)->download($file_name);
    }
}
