<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\AccidentActivity;
use Illuminate\Http\Request;

class AccidentActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $accidentactivity = AccidentActivity::where('name_de', 'LIKE', "%$keyword%")
                ->orWhere('name_fr', 'LIKE', "%$keyword%")
                ->orWhere('name_en', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $accidentactivity = AccidentActivity::latest()->paginate($perPage);
        }

        return view('admin.accident-activity.index', compact('accidentactivity'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.accident-activity.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'name_de' => 'required',
			'name_fr' => 'required'
		]);
        $requestData = $request->all();
        
        AccidentActivity::create($requestData);

        return redirect('admin/accident-activity')->with('flash_message', 'AccidentActivity added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $accidentactivity = AccidentActivity::findOrFail($id);

        return view('admin.accident-activity.show', compact('accidentactivity'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $accidentactivity = AccidentActivity::findOrFail($id);

        return view('admin.accident-activity.edit', compact('accidentactivity'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'name_de' => 'required',
			'name_fr' => 'required'
		]);
        $requestData = $request->all();
        
        $accidentactivity = AccidentActivity::findOrFail($id);
        $accidentactivity->update($requestData);

        return redirect('admin/accident-activity')->with('flash_message', 'AccidentActivity updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        AccidentActivity::destroy($id);

        return redirect('admin/accident-activity')->with('flash_message', 'AccidentActivity deleted!');
    }
}
