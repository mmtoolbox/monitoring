<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Niveau;
use Illuminate\Http\Request;

class NiveauController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $niveau = Niveau::where('name_de', 'LIKE', "%$keyword%")
                ->orWhere('name_fr', 'LIKE', "%$keyword%")
                ->orWhere('name_en', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $niveau = Niveau::latest()->paginate($perPage);
        }

        return view('admin.niveau.index', compact('niveau'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.niveau.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'name_de' => 'required',
			'name_fr' => 'required'
		]);
        $requestData = $request->all();
        
        Niveau::create($requestData);

        return redirect('admin/niveau')->with('flash_message', 'Niveau added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $niveau = Niveau::findOrFail($id);

        return view('admin.niveau.show', compact('niveau'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $niveau = Niveau::findOrFail($id);

        return view('admin.niveau.edit', compact('niveau'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'name_de' => 'required',
			'name_fr' => 'required'
		]);
        $requestData = $request->all();
        
        $niveau = Niveau::findOrFail($id);
        $niveau->update($requestData);

        return redirect('admin/niveau')->with('flash_message', 'Niveau updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Niveau::destroy($id);

        return redirect('admin/niveau')->with('flash_message', 'Niveau deleted!');
    }
}
