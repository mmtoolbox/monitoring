<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\SnowCondition;
use Illuminate\Http\Request;

class SnowConditionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $snowcondition = SnowCondition::where('name_de', 'LIKE', "%$keyword%")
                ->orWhere('name_fr', 'LIKE', "%$keyword%")
                ->orWhere('name_en', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $snowcondition = SnowCondition::latest()->paginate($perPage);
        }

        return view('admin.snow-condition.index', compact('snowcondition'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.snow-condition.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'name_de' => 'required',
			'name_fr' => 'required'
		]);
        $requestData = $request->all();
        
        SnowCondition::create($requestData);

        return redirect('admin/snow-condition')->with('flash_message', 'SnowCondition added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $snowcondition = SnowCondition::findOrFail($id);

        return view('admin.snow-condition.show', compact('snowcondition'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $snowcondition = SnowCondition::findOrFail($id);

        return view('admin.snow-condition.edit', compact('snowcondition'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'name_de' => 'required',
			'name_fr' => 'required'
		]);
        $requestData = $request->all();
        
        $snowcondition = SnowCondition::findOrFail($id);
        $snowcondition->update($requestData);

        return redirect('admin/snow-condition')->with('flash_message', 'SnowCondition updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        SnowCondition::destroy($id);

        return redirect('admin/snow-condition')->with('flash_message', 'SnowCondition deleted!');
    }
}
