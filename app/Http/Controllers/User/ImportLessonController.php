<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Lesson;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ImportLessonController extends Controller
{

    public static function importFtpFolder()
    {
        $directory = config('app.ftp_folder');
        $files = Storage::disk('local')->allFiles($directory);

        foreach ($files as $file){
                print("importing $file...\n");
                $result = self::import(storage_path('app/' . $file));
                if(count($result['errors']) === 0){
                    $filename = basename(storage_path('app/' . $file));
                    rename(storage_path('app/' . $file), storage_path() . '/app/' . $directory . '_archive/'. $filename);
                }else{
                    foreach ($result['errors'] as $error){
                        print($error);
                    }
                }
        }
    }

    public static function CsvKeys()
    {
        return [
            'user_id',
            'season_id',
            'ski_kids',
            'snowboard_kids',
            'other_kids',
            'ski_adults',
            'snowboard_adults',
            'other_adults',
            'other_private',
            'turnover_group',
            'turnover_private',
            'turnover_other',
            'total_salary',
        ];
    }

    public function index(Request $request)
    {
        return view('user.import_lessons.index');
    }

    public function upload(Request $request)
    {
        $this->validate($request, [
            'csv_file' => 'required'
        ]);

        $path = $request->file('csv_file')->getRealPath();
        $import_report = $this->import($path);

        return view('user.import_lessons.imported', $import_report);
    }

    public static function import($path)
    {
        $data = [];
        $errors = [];
        $successful = [];

        try{
            $data = array_map(function ($d) {
                return array_combine(self::CsvKeys(), array_values(str_getcsv($d, ";")));
            }, file($path));
        } catch (\ErrorException $exception){
            $errors[] = __('import.error_in_format');
        };

        foreach ($data as $i => $row) {
            $i++;
            try {
                Lesson::updateOrCreate(array_slice($row, 0, 2), $row);
                $successful[] = __('import.line_imported', ['line' => $i]);
            } catch (QueryException $exception) {
                $errors[] = __('import.error_in_line_not_imported', ['line' => $i, 'exception' => $exception->getMessage()]);
            }
        }

        return [
            'errors' => $errors,
            'successful' => $successful,
            'data' => $data
        ];
    }

}
