<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Niveau extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'niveaux';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name_de', 'name_fr', 'name_en'];

    public function accident()
    {
        return $this->hasMany('App\Accident');
    }
    
}
