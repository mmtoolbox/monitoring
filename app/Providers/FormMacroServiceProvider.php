<?php

namespace App\Providers;

use Form;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class FormMacroServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Form::macro('CheckboxGroup', function ($name, $options = [], $selected = [], $html_options) {

            $html = '';
            foreach ($options as $value => $label){
                $html .= '<div class="form-check">';
                $html .= Form::Checkbox($name,
                    $value,
                    in_array($value,$selected),
                    ['class' => 'form-check-input', 'id' => Str::slug($name . $value)]
                );
                $html .= Form::Label(Str::slug($name . $value), $label, array('class' => 'form-check-label'));
                $html .= '</div>';
            }
            return $html;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
