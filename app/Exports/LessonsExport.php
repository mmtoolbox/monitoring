<?php

namespace App\Exports;

use App\Lesson;
use Illuminate\Database\Query\Builder;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class LessonsExport implements FromQuery, WithHeadings, WithMapping
{

    use Exportable;

    /**
     * @var \Illuminate\Support\Collection
     */
    private $season_id;

    /**
     * @param int $season_id
     * @return \App\Exports\LessonsExport
     */
    public function forSeason(int $season_id)
    {
        $this->season_id = $season_id;
        return $this;
    }

    /**
     * @return Builder
     */
    public function query()
    {
        return Lesson::query()->where('season_id', $this->season_id)->orderBy('user_id');
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            __('entities.season'),
            __('entities.school'),
            __('entities.ski_kids'),
            __('entities.snowboard_kids'),
            __('entities.other_kids'),
            __('entities.ski_adults'),
            __('entities.snowboard_adults'),
            __('entities.other_adults'),
            __('content.private_lessons') . ' ' .__('entities.other_private'),
            __('content.turnover') . ' ' . __('entities.turnover_private'),
            __('content.turnover') . ' ' . __('entities.turnover_group'),
            __('content.turnover') . ' ' . __('entities.turnover_other'),
            __('content.salary_sum'),
        ];
    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($lesson): array
    {
        return [
            $lesson->season->title,
            $lesson->user->id,
            $lesson->ski_kids,
            $lesson->snowboard_kids,
            $lesson->other_kids,
            $lesson->ski_adults,
            $lesson->snowboard_adults,
            $lesson->other_adults,
            $lesson->other_private,
            $lesson->turnover_private,
            $lesson->turnover_group,
            $lesson->turnover_other,
            $lesson->total_salary,
        ];
    }
}
