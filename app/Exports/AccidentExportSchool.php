<?php

namespace App\Exports;

use App\Accident;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithDrawings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Worksheet\BaseDrawing;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class AccidentExportSchool implements FromArray, withDrawings, ShouldAutoSize, WithEvents
{

    use Exportable;
    /**
     * @var Accident
     */
    private $entity;

    public function __construct($entity)
    {
        $this->entity = $entity;
    }

    /**
     * @return Builder
     */
    public function query()
    {
        return Accident::query()->where('id', $this->id);
    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($row): array
    {
        return [$row->id];
    }

    /**
     * @return array
     */
    public function array(): array
    {
        return [
            [''],
            [__('content.accident_report')],
            [$this->entity->user->company . ' ' . $this->entity->user->name],
            [''],
            [__('content.inured_person_info')],
            [__('entities.accident_injured_lastname'), '', $this->entity->injured_lastname],
            [__('entities.accident_injured_firstname'), '', $this->entity->injured_firstname],
            [__('entities.accident_injured_birthdate'), '', $this->entity->injured_birthdate],
            [__('entities.accident_sex'), '', $this->entity->sex],
            [__('entities.accident_discipline'), '', $this->entity->discipline['name_' . Config::get('app.locale') ]],
            [__('entities.accident_niveau'), '', $this->entity->niveau['name_' . Config::get('app.locale') ]],
            [__('entities.accident_injured_street'), '', $this->entity->injured_street],
            [__('entities.accident_injured_plz') . ' ' . __('entities.accident_injured_town'), '', $this->entity->injured_plz . ' ' . $this->entity->injured_town],
            [''],
            [__('content.inured_person_holiday_address')],
            [__('entities.accident_injured_holiday_street'), '', $this->entity->injured_holiday_street],
            [__('entities.accident_injured_holiday_plz') . ' ' . __('entities.accident_injured_holiday_town'), '', $this->entity->injured_holiday_plz . ' ' . $this->entity->injured_holiday_town],
            [''],
            [__('content.accident_info')],
            [__('entities.accident_date'), '', $this->entity->date],
            [__('entities.accident_activity'), '', $this->entity->accident_activity['name_' . Config::get('app.locale') ]],
            [__('entities.accident_teacher'), '', $this->entity->teacher],
            [__('entities.accident_place'), '', $this->entity->accident_place['name_' . Config::get('app.locale') ]],
            [__('entities.accident_weather'), '', $this->entity->weather['name_' . Config::get('app.locale') ]],
            [__('entities.accident_snow_conditions'), '', $this->entity->snow_condition['name_' . Config::get('app.locale') ]],
            [__('entities.accident_gravity'), '', $this->entity->accident_gravity['name_' . Config::get('app.locale') ]],
            [__('entities.accident_description'), '', $this->entity->description],
            [__('entities.accident_body_part'), '', $this->entity->body_parts_list],
            [__('entities.accident_injury_type'), '', $this->entity->injury_type_list],
            [__('entities.accident_protection'), '', $this->entity->protection['name_' . Config::get('app.locale') ]],
            [__('entities.accident_piste_rescue'), '', $this->entity->piste_rescue['name_' . Config::get('app.locale') ]],
            [__('entities.accident_witnesses'), '', $this->entity->witnesses],
            [__('entities.accident_description'), '', $this->entity->description],
            [''],
            [''],
            [__('content.venue_date'), '', __('content.signature_prof')]

        ];
    }

    /**
     * @return BaseDrawing|BaseDrawing[]
     */
    public function drawings()
    {
        $drawing = new Drawing();
        $drawing->setName('Logo');
        $drawing->setDescription('Swiss Snowsports Logo');
        $drawing->setPath(public_path('img/logo_sss_sw.png'));
        $drawing->setHeight(90);
        $drawing->setCoordinates('A1');

        return $drawing;
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {

                // Title
                $cellRange = 'A3';
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(24);
                $event->sheet->mergeCells('A3:C3');

                // Sub-title
                $cellRange = 'A4';
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(18);
                $event->sheet->mergeCells('A4:C4');

                // headings
                $event->sheet->getDelegate()->getStyle('A6')->getFont()->setSize(14);
                $event->sheet->mergeCells('A6:C6');
                $event->sheet->getDelegate()->getStyle('A20')->getFont()->setSize(14);
                $event->sheet->mergeCells('A20:C20');

                // headings 2
                $event->sheet->getDelegate()->getStyle('A16')->getFont()->setSize(12);
                $event->sheet->mergeCells('A16:C16');

                // logo row height
                $event->sheet->getRowDimension('1')->setRowHeight(75);

                // signature lines
                $styleArray = [
                    'borders' => [
                        'bottom' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                    ],
                ];
                $event->sheet->getStyle('A36')->applyFromArray($styleArray);
                $event->sheet->getStyle('C36')->applyFromArray($styleArray);

                // spacing column
                $event->sheet->getColumnDimension('B')->setWidth(2);
            },
        ];
    }
}
