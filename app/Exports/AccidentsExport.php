<?php

namespace App\Exports;

use App\Accident;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\Config;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class AccidentsExport implements FromQuery, WithHeadings, WithMapping
{
    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            __('entities.school'),
            __('entities.accident_date'),
            __('entities.accident_title'),
            __('entities.accident_place'),
            __('entities.accident_activity'),
            __('entities.accident_teacher'),
            __('entities.accident_weather'),
            __('entities.accident_snow_conditions'),
            __('entities.accident_type'),
            __('entities.accident_witnesses'),
            __('entities.accident_piste_rescue'),
            __('entities.accident_description'),
            __('entities.accident_gravity'),
            __('entities.accident_protection'),
            __('entities.accident_body_part'),
            __('entities.accident_injury_type'),
            __('entities.accident_injured_person'),
            __('entities.accident_age_category'),
            __('entities.accident_sex'),
            __('entities.accident_discipline'),
            __('entities.accident_niveau'),
            __('entities.accident_injured_lastname'),
            __('entities.accident_injured_firstname'),
            __('entities.accident_injured_birthdate'),
            __('entities.accident_injured_street'),
            __('entities.accident_injured_plz'),
            __('entities.accident_injured_town'),
            __('entities.accident_injured_country'),
            __('entities.accident_injured_holiday_street'),
            __('entities.accident_injured_holiday_plz'),
            __('entities.accident_injured_holiday_town'),
        ];
    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($accident): array
    {
        return [
            $accident->user->name,
            \Carbon\Carbon::parse($accident->date)->format('d.m.Y'),
            $accident->title,
            $accident->accident_place['name_' . Config::get('app.locale') ],
            $accident->accident_activity['name_' . Config::get('app.locale') ],
            $accident->accident_teacher,
            $accident->weather['name_' . Config::get('app.locale') ],
            $accident->snow_condition['name_' . Config::get('app.locale') ],
            $accident->accident_type['name_' . Config::get('app.locale') ],
            $accident->accident_witnesses,
            $accident->piste_rescue['name_' . Config::get('app.locale') ],
            $accident->description,
            $accident->accident_gravity['name_' . Config::get('app.locale') ],
            $accident->protection['name_' . Config::get('app.locale') ],
            $accident->body_parts_list,
            $accident->injury_type_list,
            $accident->injured_person_str,
            $accident->age_category['name'],
            $accident->sex,
            $accident->discipline['name_' . Config::get('app.locale') ],
            $accident->niveau['name_' . Config::get('app.locale') ],
            $accident->injured_lastname,
            $accident->injured_firstname,
            $accident->injured_birthdate,
            $accident->injured_street,
            $accident->injured_plz,
            $accident->injured_town,
            $accident->injured_country,
            $accident->injured_holiday_street,
            $accident->injured_holiday_plz,
            $accident->injured_holiday_town,
        ];
    }

    /**
     * @return Builder
     */
    public function query()
    {
        return Accident::query()->with(['user', 'accident_place', 'accident_activity', 'weather', 'snow_condition', 'accident_type', 'piste_rescue', 'accident_gravity', 'age_category', 'discipline', 'niveau'])->orderBy('date', 'desc');
    }
}
