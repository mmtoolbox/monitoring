<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Accident extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'accidents';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    //protected $dates = ['date'];

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'date', 'accident_place_id', 'accident_activity_id', 'teacher', 'weather_id', 'snow_conditions_id', 'accident_type_id', 'witnesses', 'piste_rescue_id', 'description', 'accident_gravity_id', 'protection_id', 'injured_person', 'age_category_id', 'sex', 'discipline_id', 'niveau_id', 'injured_lastname', 'injured_firstname', 'injured_birthdate', 'injured_street', 'injured_plz', 'injured_town', 'injured_country', 'injured_holiday_street', 'injured_holiday_plz', 'injured_holiday_town', 'user_id'];

    public function getDateHumanAttribute($date)
    {
        return Carbon::parse($date)->format('d.m.Y');
    }

    public function accident_place()
    {
        return $this->belongsTo('App\AccidentPlace');
    }
    public function accident_activity()
    {
        return $this->belongsTo('App\AccidentActivity');
    }
    public function weather()
    {
        return $this->belongsTo('App\Weather');
    }
    public function snow_condition()
    {
        return $this->belongsTo('App\SnowCondition', 'snow_conditions_id');
    }
    public function accident_type()
    {
        return $this->belongsTo('App\AccidentType');
    }
    public function piste_rescue()
    {
        return $this->belongsTo('App\PisteRescue');
    }
    public function accident_gravity()
    {
        return $this->belongsTo('App\AccidentGravity');
    }
    public function protection()
    {
        return $this->belongsTo('App\Protection');
    }
    public function body_parts()
    {
        return $this->belongsToMany('App\BodyPart');
    }
    public function injury_types()
    {
        return $this->belongsToMany('App\InjuryType');
    }
    public function getBodyPartsListAttribute()
    {
        return implode(', ', $this->body_parts()->pluck('name_' . App::getLocale())->toArray());
    }
    public function getInjuryTypeListAttribute()
    {
        return implode(', ', $this->injury_types()->pluck('name_' . App::getLocale())->toArray());
    }
    public function getInjuredPersonStrAttribute()
    {
        return $this->injured_person === 'guest' ? __('entities.guest') : __('entities.teacher');
    }
    public function age_category()
    {
        return $this->belongsTo('App\AgeCategory');
    }
    public function discipline()
    {
        return $this->belongsTo('App\Discipline');
    }
    public function niveau()
    {
        return $this->belongsTo('App\Niveau');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
