<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgeCategory extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'age_categories';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    public function accident()
    {
        return $this->hasMany('App\Accident');
    }
    
}
