<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BodyPart extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'body_parts';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name_de', 'name_fr', 'name_en'];

    public function accidents()
    {
        return $this->belongsToMany('App\Accident');
    }

}
