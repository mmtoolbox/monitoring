<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home_full', 'HomeController@index_full')->name('home_full');


Route::group(['namespace' => 'Admin',  'middleware' => ['auth', 'can:isAdmin'], 'prefix' => 'admin'], function (){

    /**
     *      Admin routes
     */
    Route::get('/accident/export', 'AccidentController@export')->name('accident_export');
    Route::get('/lessons/export', 'LessonsController@export')->name('lesson_export');

    Route::resource('user', 'UserController');
    Route::resource('accident', 'AccidentController');
    Route::resource('seasons', 'SeasonsController');
    Route::resource('lessons', 'LessonsController');
    Route::resource('niveau', 'NiveauController');
    Route::resource('accident-place', 'AccidentPlaceController');
    Route::resource('weather', 'WeatherController');
    Route::resource('snow-condition', 'SnowConditionController');
    Route::resource('accident-type', 'AccidentTypeController');
    Route::resource('piste-rescue', 'PisteRescueController');
    Route::resource('accident-gravity', 'AccidentGravityController');
    Route::resource('protection', 'ProtectionController');
    Route::resource('injury-type', 'InjuryTypeController');
    Route::resource('body-part', 'BodyPartController');
    Route::resource('discipline', 'DisciplineController');
    Route::resource('age-category', 'AgeCategoryController');
    Route::resource('accident-activity', 'AccidentActivityController');

});

Route::group(['namespace' => 'User',  'middleware' => ['auth', 'can:isSchool'], 'prefix' => 'user'], function (){

    /**
     *      User (=school) routes
     */

    Route::get('/lessons/{id}/export', 'LessonsController@export')->name('export_lesson');
    Route::get('/accident/{id}/export', 'AccidentController@export')->name('export_accident');

    Route::resource('lessons', 'LessonsController');
    Route::resource('accident', 'AccidentController');
    Route::get('/import', 'ImportLessonController@index')->name('import');
    Route::post('/import/upload', 'ImportLessonController@upload')->name('import_upload');
});

/**
 *      change locale
 */
Route::get('lang/{locale}', function ($locale) {
    if (in_array($locale, \Config::get('app.locales'))) {
        session(['locale' => $locale]);
    }
    return redirect()->back();
})->name('language');
