<?php

use Illuminate\Database\Seeder;

class EncryptPasswordsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = DB::table('users')->get();

        foreach ($users as $user){
            DB::table('users')
                ->where('id', $user->id)
                ->update(['password' => bcrypt($user->password)]);
        }
    }
}
