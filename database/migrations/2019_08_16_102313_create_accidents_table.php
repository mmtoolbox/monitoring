<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAccidentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accidents', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('title')->nullable();
            $table->date('date')->nullable();
            $table->integer('accident_place_id')->unsigned();
            $table->integer('accident_activity_id')->unsigned();
            $table->string('teacher')->nullable();
            $table->integer('weather_id')->unsigned();
            $table->integer('snow_conditions_id')->unsigned();
            $table->integer('accident_type_id')->unsigned();
            $table->string('witnesses')->nullable();
            $table->integer('piste_rescue_id')->unsigned();
            $table->text('description')->nullable();
            $table->integer('accident_gravity_id')->unsigned();
            $table->integer('protection_id')->unsigned();
            $table->string('injured_person');
            $table->integer('age_category_id')->unsigned();
            $table->string('sex');
            $table->integer('discipline_id')->unsigned();
            $table->integer('niveau_id')->unsigned();
            $table->string('injured_lastname')->nullable();
            $table->string('injured_firstname')->nullable();
            $table->date('injured_birthdate')->nullable();
            $table->string('injured_street')->nullable();
            $table->integer('injured_plz')->unsigned();
            $table->string('injured_town')->nullable();
            $table->string('injured_country')->nullable();
            $table->string('injured_holiday_street')->nullable();
            $table->integer('injured_holiday_plz')->unsigned();
            $table->string('injured_holiday_town')->nullable();
            $table->foreign('accident_place_id')->references('id')->on('accident_places')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('accident_activity_id')->references('id')->on('accident_activities')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('weather_id')->references('id')->on('weathers')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('snow_conditions_id')->references('id')->on('snow_conditions')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('accident_type_id')->references('id')->on('accident_types')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('piste_rescue_id')->references('id')->on('piste_rescues')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('accident_gravity_id')->references('id')->on('accident_gravities')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('protection_id')->references('id')->on('protections')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('age_category_id')->references('id')->on('age_categories')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('discipline_id')->references('id')->on('disciplines')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('niveau_id')->references('id')->on('niveaux')->onDelete('restrict')->onUpdate('restrict');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('accidents');
    }
}
