<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFieldsInAgeCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('age_categories', function (Blueprint $table) {
            $table->dropColumn(['name_de', 'name_fr', 'name_en']);
            $table->string('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('age_categories', function (Blueprint $table) {
            $table->dropColumn(['name']);
            $table->string('name_de');
            $table->string('name_fr');
            $table->string('name_en');
        });
    }
}
