<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeLessonsNumbersToDecimals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lessons', function (Blueprint $table) {
            $table->decimal('ski_kids', 9,2)->nullable()->change();
            $table->decimal('snowboard_kids', 9,2)->nullable()->change();
            $table->decimal('other_kids', 9,2)->nullable()->change();
            $table->decimal('ski_adults', 9,2)->nullable()->change();
            $table->decimal('snowboard_adults', 9,2)->nullable()->change();
            $table->decimal('other_adults', 9,2)->nullable()->change();
            $table->decimal('other_private', 9,2)->nullable()->change();
            $table->decimal('turnover_group', 9,2)->nullable()->change();
            $table->decimal('turnover_private', 9,2)->nullable()->change();
            $table->decimal('turnover_other', 9,2)->nullable()->change();
            $table->decimal('total_salary', 9,2)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lessons', function (Blueprint $table) {
            $table->integer('ski_kids')->nullable();
            $table->integer('snowboard_kids')->nullable();
            $table->integer('other_kids')->nullable();
            $table->integer('ski_adults')->nullable();
            $table->integer('snowboard_adults')->nullable();
            $table->integer('other_adults')->nullable();
            $table->integer('other_private')->nullable();
            $table->integer('turnover_private')->nullable();
            $table->integer('turnover_group')->nullable();
            $table->integer('turnover_other')->nullable();
            $table->integer('total_salary')->nullable();
        });
    }
}
