<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakePlzsNullableAccidentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accidents', function (Blueprint $table) {
            $table->integer('injured_plz')->unsigned()->nullable()->change();
            $table->integer('injured_holiday_plz')->unsigned()->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accidents', function (Blueprint $table) {
            $table->integer('injured_plz')->unsigned()->nullable(false)->change();
            $table->integer('injured_holiday_plz')->unsigned()->nullable(false)->change();
        });
    }
}
