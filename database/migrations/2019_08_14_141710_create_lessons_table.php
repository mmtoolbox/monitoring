<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLessonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lessons', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('ski_kids')->nullable();
            $table->integer('snowboard_kids')->nullable();
            $table->integer('other_kids')->nullable();
            $table->integer('ski_adults')->nullable();
            $table->integer('snowboard_adults')->nullable();
            $table->integer('other_adults')->nullable();
            $table->integer('other_private')->nullable();
            $table->integer('turnover_private')->nullable();
            $table->integer('turnover_group')->nullable();
            $table->integer('turnover_other')->nullable();
            $table->integer('total_salary')->nullable();
            $table->integer('season_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('season_id')->references('id')->on('seasons')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lessons');
    }
}
