<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAccidentActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accident_activities', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('name_de')->nullable();
            $table->string('name_fr')->nullable();
            $table->string('name_en')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('accident_activities');
    }
}
