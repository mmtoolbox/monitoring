<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccidentInjuryTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accident_injury_type', function (Blueprint $table) {
            $table->unsignedInteger('accident_id');
            $table->unsignedInteger('injury_type_id');
            $table->primary(['accident_id', 'injury_type_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('accident_injury_type');
    }
}
