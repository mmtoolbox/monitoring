<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccidentBodyPartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accident_body_part', function (Blueprint $table) {
            $table->unsignedInteger('accident_id');
            $table->unsignedInteger('body_part_id');
            $table->primary(['accident_id','body_part_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('accident_body_part');
    }
}
