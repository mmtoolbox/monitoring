SELECT `id_person_unfall` as accident_id,
CONCAT_WS(',',
CASE WHEN kopf_hals IS NULL THEN NULL ELSE '1' END,
CASE WHEN rumpf_wirbelsaeule IS NULL THEN NULL ELSE '2' END,
CASE WHEN schulter_oberarm IS NULL THEN NULL ELSE '3' END,
CASE WHEN ellbogen_vorderarm IS NULL THEN NULL ELSE '4' END,
CASE WHEN handgelenk_hand IS NULL THEN NULL ELSE '5' END,
CASE WHEN huefte_oberschenkel IS NULL THEN NULL ELSE '6' END,
CASE WHEN knie IS NULL THEN NULL ELSE '7' END,
CASE WHEN unterschenkel_sprungelenk_fuss IS NULL THEN NULL ELSE '8' END,
CASE WHEN innereverletzungen IS NULL THEN NULL ELSE '9' END
)
 as body_part_id FROM `PersonUnfall` as `accident_body_part_transfer` WHERE 1 ORDER BY `body_part_id` ASC

-- Create table `accident_body_part_transfer`
-- Export and import above query to `accident_body_part_transfer`
-- Normalize values below and insert values into `accident_body_part`

SELECT
accident_body_part_transfer.accident_id,
SUBSTRING_INDEX(SUBSTRING_INDEX(accident_body_part_transfer.body_part_id, ',', body_parts.id), ',', -1) as body_part_id
from body_parts
INNER JOIN accident_body_part_transfer
ON CHAR_LENGTH(accident_body_part_transfer.body_part_id)
     -CHAR_LENGTH(REPLACE(accident_body_part_transfer.body_part_id, ',', ''))>=body_parts.id-1
WHERE body_part_id NOT LIKE ''
ORDER BY
  accident_id, body_part_id
