SELECT `id_unterricht` as id,
`a_num_schule` as `user_id`,
CASE
    WHEN (datum_unterricht >= '2015-11-01' AND datum_unterricht <= '2016-06-30') THEN 1
    WHEN (datum_unterricht >= '2016-11-01' AND datum_unterricht <= '2017-06-30') THEN 2
    WHEN (datum_unterricht >= '2017-11-01' AND datum_unterricht <= '2018-06-30') THEN 3
    WHEN (datum_unterricht >= '2018-07-01' AND datum_unterricht <= '2019-06-30') THEN 4
    ELSE 0
  END AS season_id,
SUM(`ski_kids`) as `ski_kids`,
SUM(`snowboard_kids`) as `snowboard_kids`,
SUM(`andere_kids`) as `other_kids`,
SUM(`ski_erw`) as `ski_adults`,
SUM(`snowboard_erw`) as `snowboard_adults`,
SUM(`andere_erw`) as `other_adults`,
SUM(`andere_privat`) as `other_private`,
SUM(`umsatz_gruppe`) as `turnover_group`,
SUM(`umsatz_privat`) as `turnover_private`,
SUM(`umsatz_andere`) as `turnover_other`,
SUM(`lohn_gesamt`) as `total_salary`
FROM `Unterricht`
GROUP BY `user_id`, season_id
HAVING season_id>0
